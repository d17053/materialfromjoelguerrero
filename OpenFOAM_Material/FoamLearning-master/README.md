# FoamLearning
This repository is used as a record of OpenFoam learning everyday. As my pervious background is on wind tunnel experiments and the learnign process is a little tough for me. Thus, I push this file as a log of daliy progress.

## Tfd.chalmers.se

[Link-1](http://www.tfd.chalmers.se/~hani/kurser/OS_CFD_2017/) is the step-by-step course for master and PhD student. [Link-2](http://www.tfd.chalmers.se/~hani/kurser/OS_CFD/) has several reports with detailed OpenFOAM applications combining Maltab, pyhton and so on.



## Joel Guerrero's Tutorials 
[Link-1](http://www.dicat.unige.it/guerrero/python1.html) is a introduction for four tutorials. [Link-2](https://github.com/joelguerrero/first-tutorial/) is git-hub source code for using python to handle OpenFOAM data.

### Hagen-Poiseuille flow
The OpenFOAM directory of planar Poiseuille is *$FOAM_TUTORIALS/incompressible/pimpleFoam/laminar/planarPoiseuille/*, which could be obtain by typing in ubuntu terminal:

    grep -r Poiseuille $FOAM_TUTORIALS


## Helpful Blog

1. [Dr. Mohamed Sukri Mat Ali](http://staff.blog.utm.my/dr-sukri/tag/openfoam/) shared his openFoam points on this blog, it is help for beginner.
