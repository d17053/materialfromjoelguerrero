#!/bin/sh
#simple script to show do-while loop

i=0
end=10

while [ $i -lt $end ]
#until [ $i -ge $end ]
#until [ $i -eq $end ]

do
	echo "$i"
#Added two parenthese because there is a variable the needs to evaluated
	i=$(($i + 1))
done
