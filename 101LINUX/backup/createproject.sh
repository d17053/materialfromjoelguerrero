#!/bin/bash
#Create a sample project structure

echo "Creating backups directory"
mkdir backups

echo "Creating projects directory"
mkdir projects
cd ./projects

echo "Creating projects/task1 directory"
mkdir task1
cd ./task1

echo "Creating files inside the projects/task1 directory"
ls -lR /usr/ > info.txt 2> errors.txt

echo "End of task"
