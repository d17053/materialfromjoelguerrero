#!/bin/bash
# A simple demonstration of default variables in bash
# It takes arguments

echo My name is $0 and I have been given $# command line arguments
echo Here they are: $*
echo And the 2nd command line argument is $2
