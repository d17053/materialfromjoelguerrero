#include <iostream> 

using namespace std;

int addition (int a, int b) 					//function definition
{ 
	int c;							//function statements
	c=a+b; 							//function statements
	return (c); 						//function statements
}

int main () 
{ 
	int answer; 
		
	answer = addition (5,3); 				//call to function addition
	cout << "The result is " << answer << endl; 
	return 0; 
}
