#!/bin/bash

echo "Cleaning directories ..."

echo "	Cleaning lib/"
rm lib/*.a	> /dev/null 2>&1
rm lib/*.so	> /dev/null 2>&1

echo "	Cleaning obj/"
rm obj/*.o	> /dev/null 2>&1

echo "	Cleaning executables in ."
rm run*		> /dev/null 2>&1

echo "Finished cleaning directory ..."