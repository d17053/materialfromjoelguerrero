#include <iostream> 
#include "function_addition0.H"

using namespace std;

/*
int division (int a, int b) 				
{ 
	int c;						
	c = a/b; 					
	return (c); 					
}
*/

/*
template <class T>
T division (T a, T b) 				
{ 
	T c;						
	c = a/b; 					
	return (c); 					
}
*/

int main () 
{ 
/*
	int answer; 	
	answer = division (1.,0); 				
	cout << "The result is " << answer << endl; 
	return 0;
*/
	int answer_int; 
	answer_int = division <int> (1,1);
	cout << "The result is " << answer_int << endl; 

	double answer_double; 
	answer_double = division <double> (7.47524,12.30);
	cout << "The result is " << answer_double << endl; 
}
